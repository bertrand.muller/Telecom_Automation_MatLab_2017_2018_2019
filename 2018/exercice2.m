T1 = 3;
T2 = 2;
Tz = 1;
Tau = 2;

s = tf('s');

% Système r
% Retardé par rapport à sys1 (terme exp(-tau*s))
% Tau -> Retard pur
% Système à retard pur -> système dont la réponse est décalée dans le temps
% (Ex: Phénomènes de transport de matière avec les transporteuses)
sysr = exp(-Tau*s)/(1+s*T1);
% Modification du retard: sysr.OutputDelay = 15;
subplot(3,3,1)
step(sysr)
title('sysr')

% Système 2
% Système du second ordre
% Résultat de la somme de deux systèmes du 1er ordre en cascade
% Système à pôles réels
sys2 = 1/((1+s*T1)*(1+s*T2));
subplot(3,3,2)
step(sys2)
title('sys2')

% Système 3
% Système pseudo-périodique amorti
% Gain statique de la fonction de transfert est de 2
% Système à pôles complexes conjugués. Partie réelle positive -> stable
sys3 = 2*tf([Tz 1], [1 0.2 1]);
subplot(3,3,3)
step(sys3)
title('sys3')
[coefSys3,polesSys3,kSys3] = residue(sys3.Numerator{1}, sys3.Denominator{1});
damp(sys3); % Donne les pôles et le coefficient d'amortissement

% Système 1
% Système du 1er ordre
% Constante de temps: T1
% Tangente à l'origine non nulle
sys1 = 1/(1+s*T1);
subplot(3,3,4)
step(sys1)
title('sys1')

% Système 2.b
% système du 2nd ordre
% Constantes de temps: T1 & T2
% Système plus lent car chaque décomposition en systèmes du 1er ordre
% a sa propre constante de temps
k = 1;
sys2b = 1/((1+s*T1)*(1+s*(T2/k)));
subplot(3,3,5)
step(sys2b)
title('sys2b')

% Système 3.b
sys3b = 2*tf([Tz 1], [1 0.2 2]);
subplot(3,3,6)
step(sys3b)
title('sys3b')

% Système p
sysp = (sys2*sys3) / (1+sys2*sys3);
subplot(3,3,7)
step(sysp)
title('sysp')
[coefSysp,polesSysp,kSysp] = residue(sysp.Numerator{1}, sysp.Denominator{1});

% Comparaison de sys1 & sys2b avec k = 1
% subplot(3,3,6)
% stepplot(sys1,sys2b)
% title('Comparaison de sys1 & sys2b / K= 1')

% Comparaison de sys1 & sys2b avec k = 2
% k = 2;
% sys2b = 1/((1+s*T1)*(1+s*(T2/k)));
% subplot(3,3,7)
% stepplot(sys1,sys2b)
% title('Comparaison de sys1 & sys2b / K= 2')

% Comparaison de sys1 & sys2b avec k = 5
k = 5;
sys2b = 1/((1+s*T1)*(1+s*(T2/k)));
subplot(3,3,8)
stepplot(sys1,sys2b)
title('Comparaison de sys1 & sys2b / K= 5')

% Comparaison de sys1 & sys2b avec k = 10
% Courbes très proches du signal sys1 puisque la constante de temps T1 est
% plus importante que la constante de T2/k
k = 10;
sys2b = 1/((1+s*T1)*(1+s*(T2/k)));
subplot(3,3,9)
stepplot(sys1,sys2b)
title('Comparaison de sys1 & sys2b / K= 10')

% !! Autres méthodes utiles !!
% dcgain -> poru avoir le gain statique
% tfdata -> pour avoir le numérateur et le dénominateur d'une fonction
% zpk -> factorise la fonction de transfert (pôles)
% pzmap -> affiche les zéros et les pôles de manière graphique

sysz = zpk(tf([3 1], [1 3 2]));

% Les rayons grid représentent les impulsions pures w0
figure;
pzmap(sys2,sys3,sys3b)
grid
axis equal

figure 
pzmap(sysp)
grid
axis equal