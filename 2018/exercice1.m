
% 2 racines réelles => donc peut être décomposé en 2 systèmes du 1er ordre

s = tf('s');
ys = (3*s+2) / (4*s^2+8*s+1);
% Autre méthode: ys = tf([3 2], [4 8 1]);


% Calcul des pôles et des zéros
pole(ys)
zero(ys)


% Calcul de la transformée de Laplace inverse
% On veut retrouver l'original temporel
syms s t;
y = (3*s+2)/(4*s^2+8*s+1);
ilaplace(y)
ilaplace(y, s, t) % Autre méthode

latex(ilaplace(y));
ezplot(ilaplace(y))

% Réponse impulsionnelle
impulse(ys);

% Décomposition en éléments simples avec les coefficients et les pôles
[coef,poles,k] = residue(ys.Numerator{1}, ys.Denominator{1});

% Autre méthode pour retrouver l'original temporel
t = 0:0.1:5;
yy = coef(1)*exp(poles(1)*t) + coef(2)*exp(poles(2)*t);
plot(t,yy)

