s = tf('s');
K = 10; % Gain statique
T = 2; % en s

% Fonctions de transfert
% k = numérateur / [T 1] = dénominateur
F = tf(K, [T 1])

% Calcul des pôles de G et F
% Autre solution: roots([T 1])
p = pole(G);

get(G);
get(tf);

% Get la taille de la matrice
G.num;
G.den;

% Get le dénominateur
G.den{1};
DenG = get(G,'den');
DenG(1)